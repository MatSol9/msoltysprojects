package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CooperationSummaryNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

public class SummaryFrame extends JFrame {
    SummaryFrame(String companyId, CooperationSummaryNode cooperationSummaryNode){
        super("Szczegóły raportu");
        this.companyId = companyId;
        this.cooperationSummaryNode = cooperationSummaryNode;
        initGraphicalComponents();
        this.add(mainPanel);
        this.setIconImage(GlobalOptions.img.getImage());

        this.responsiblePersonPanel = new JPanel();
        this.companyPanel = new JPanel();
        this.editTimePanel = new JPanel();

        this.responsiblePersonPanel.setLayout(new BorderLayout());
        this.responsiblePersonPanel.add(new JLabel("Autor"), BorderLayout.NORTH);
        this.responsiblePersonField = new JTextField();
        this.responsiblePersonPanel.add(responsiblePersonField, BorderLayout.CENTER);

        this.companyPanel.setLayout(new BorderLayout());
        this.companyPanel.add(new JLabel("Firma"), BorderLayout.NORTH);
        this.companyField = new JTextField();
        this.companyPanel.add(companyField, BorderLayout.CENTER);

        this.editTimePanel.setLayout(new BorderLayout());
        this.editTimePanel.add(new JLabel("Data raportu"), BorderLayout.NORTH);
        this.editDateField = new JTextField();
        this.editTimePanel.add(editDateField, BorderLayout.CENTER);
        this.editDateField.setEditable(false);

        this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        this.mainPanel.add(editTimePanel);
        this.mainPanel.add(datePanel);
        this.mainPanel.add(companyPanel);
        this.mainPanel.add(responsiblePersonPanel);
        this.mainPanel.add(resultPanel);
        this.mainPanel.add(summaryPanel);
        this.mainPanel.add(buttonPanel);

        this.buttonPanel.add(saveButton);
        this.buttonPanel.add(editButton);
        this.buttonPanel.add(cancelButton);
        this.editButton.addActionListener(this::actionPerformed);

        this.setEditable(false);
        this.yearField.setEditable(false);
        this.companyField.setEditable(false);
        this.responsiblePersonField.setEditable(false);
        this.saveButton.setEnabled(false);

        this.pack();

        String dateString = this.cooperationSummaryNode.getEditTime();
        if(LocalDateTime.parse(dateString).getMinute() < 10){
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":0" + LocalDateTime.parse(dateString).getMinute();
        }
        else{
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":" + LocalDateTime.parse(dateString).getMinute();
        }

        this.editDateField.setText(dateString);
        this.yearField.setText(this.cooperationSummaryNode.getYear().toString());
        this.companyField.setText(this.cooperationSummaryNode.getCompany().getName());
        this.resultField.setText(this.cooperationSummaryNode.getResults());
        this.responsiblePersonField.setText(this.cooperationSummaryNode.getResponsiblePerson().getName() + " " + this.cooperationSummaryNode.getResponsiblePerson().getSurname());

        try {
            this.summaryArea.setText(URLDecoder.decode(this.cooperationSummaryNode.getSummary(), StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

    SummaryFrame(String companyId){
        super("Nowy raport");
        this.companyId = companyId;
        this.setIconImage(GlobalOptions.img.getImage());
        initGraphicalComponents();
        this.setLayout(new BorderLayout());
        this.add(mainPanel, BorderLayout.NORTH);
        this.mainPanel.setLayout(new BorderLayout());
        this.mainPanel.add(datePanel, BorderLayout.NORTH);
        this.mainPanel.add(resultPanel, BorderLayout.CENTER);
        this.mainPanel.add(summaryPanel, BorderLayout.SOUTH);
        this.buttonPanel.add(saveButton);
        this.buttonPanel.add(cancelButton);
        this.add(buttonPanel, BorderLayout.CENTER);
        this.pack();
    }

    public void initGraphicalComponents(){
        this.mainPanel = new JPanel();
        this.datePanel = new JPanel();
        this.resultPanel = new JPanel();
        this.summaryPanel = new JPanel();
        this.buttonPanel = new JPanel();

        this.buttonPanel.setLayout(new GridLayout());

        this.datePanel.setLayout(new BorderLayout());
        this.datePanel.add(new JLabel("Edycja"), BorderLayout.NORTH);
        this.yearField = new JTextField();
        this.datePanel.add(yearField, BorderLayout.CENTER);

        this.resultPanel.setLayout(new BorderLayout());
        this.resultPanel.add(new JLabel("Rezultat"), BorderLayout.NORTH);
        this.resultField = new JTextField();
        this.resultPanel.add(resultField, BorderLayout.CENTER);

        this.summaryPanel.setLayout(new BorderLayout());
        this.summaryPanel.add(new JLabel("Podsumowanie"), BorderLayout.NORTH);
        this.summaryArea = new JTextArea(40, 40);
        this.summaryArea.setLineWrap(true);
        this.summaryPanel.add(summaryArea, BorderLayout.CENTER);

        this.saveButton.addActionListener(this::actionPerformed);
        this.cancelButton.addActionListener(this::actionPerformed);
    }

    private void setEditable(boolean value){
        this.resultField.setEditable(value);
        this.summaryArea.setEditable(value);
    }

    private String companyId;
    private CooperationSummaryNode cooperationSummaryNode = null;

    private JPanel mainPanel;
    private JPanel datePanel;
    private JPanel resultPanel;
    private JPanel summaryPanel;
    private JPanel buttonPanel;
    private JPanel responsiblePersonPanel;
    private JPanel companyPanel;
    private JPanel editTimePanel;

    private JTextField yearField;
    private JTextField resultField;
    private JTextField responsiblePersonField;
    private JTextField companyField;
    private JTextField editDateField;

    private JTextArea summaryArea;

    private JButton saveButton = new JButton("Zapisz");
    private JButton cancelButton = new JButton("Anuluj");
    private JButton editButton = new JButton("Edytuj");

    private void actionPerformed(ActionEvent e){
        if(e.getSource() == this.saveButton && this.cooperationSummaryNode == null){
            try {
                ServerCommunication.mutations.cooperationSummaryCreate(companyId, resultField.getText(), URLEncoder.encode(this.summaryArea.getText(), StandardCharsets.UTF_8.toString()), Integer.parseInt(this.yearField.getText()));
                dispose();
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex.getCause());
            }
        }
        else if(e.getSource() == this.cancelButton){
            dispose();
        }
        else if(e.getSource() == this.editButton){
            this.setEditable(true);
            this.saveButton.setEnabled(true);
            this.editButton.setEnabled(false);
        }
        else if(e.getSource() == this.saveButton){
            this.setEditable(false);
            this.saveButton.setEnabled(false);
            this.editButton.setEnabled(true);
            try {
                ServerCommunication.mutations.cooperationSummaryUpdate(this.cooperationSummaryNode.getId(), resultField.getText(), URLEncoder.encode(this.summaryArea.getText(), StandardCharsets.UTF_8.toString()));
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex.getCause());
            }
        }
    }

}
