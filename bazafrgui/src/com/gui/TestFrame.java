package com.gui;

import com.servercommunication.graphobjects.ContactHistoryNode;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class TestFrame extends JFrame {
    public TestFrame(ArrayList<ContactHistoryNode> history){

        super(GlobalOptions.superString);
        Toolkit t = Toolkit.getDefaultToolkit();
        this.setBounds(0, 0, t.getScreenSize().width, t.getScreenSize().height - 50);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        this.setVisible(true);
        System.out.println(history.get(0).getNotes());
    }
}
