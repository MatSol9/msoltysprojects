package com.gui;

import com.servercommunication.graphobjects.CompanyNode;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class CompaniesList extends JPanel {

    private final MainFrame motherFrame;

    public CompaniesList(MainFrame motherFrame) {
        super();
        this.motherFrame = motherFrame;
        this.setLayout(new GridLayout(1000, 1));

    }

    public void displayNewList() {
        while (this.getComponentCount() > 0) {
            this.remove(this.getComponent(this.getComponentCount() - 1));
        }
        CopyOnWriteArrayList<CompanyNode> results =  motherFrame.getResults();
        this.setLayout(new GridLayout(Math.max(results.size(),10), 1));
        for (CompanyNode node : results) {
            this.add(new CompanyPanel(node, motherFrame));
        }
    }

    public void loadingPanel(){
        while (this.getComponentCount() > 0) {
            this.remove(this.getComponent(this.getComponentCount() - 1));
        }
        this.add(new JLabel("Wczytywanie..."));
    }
}
