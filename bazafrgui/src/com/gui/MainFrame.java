package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CompanyNode;
import com.servercommunication.graphobjects.StaffNode;
import org.apache.batik.transcoder.TranscoderException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Pattern;


public class MainFrame extends JFrame {
    public MainFrame(StaffNode me) {
        super(GlobalOptions.superString);
        Toolkit t = Toolkit.getDefaultToolkit();
        this.setBounds(0, 0, t.getScreenSize().width, t.getScreenSize().height - 50);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(new BorderLayout());
        this.setVisible(true);
        this.setJMenuBar(mb);
        this.me=me;
        this.setIconImage(GlobalOptions.img.getImage());
        initGraphicalComponents();
    }

    private void initGraphicalComponents() {
        this.add(mainPanel, BorderLayout.NORTH);
        mainPanel.setLayout(new BorderLayout());

        closeProgram.addActionListener(this::actionPerformed);
        programMenu.add(closeProgram);

        about.addActionListener(this::actionPerformed);
        programMenu.add(about);

        addCompany.addActionListener(this::actionPerformed);
        companiesMenu.add(addCompany);

        myCompanies.addActionListener(this::actionPerformed);
        companiesMenu.add(myCompanies);

        showProfile.addActionListener(this::actionPerformed);
        userInfoMenu.add(showProfile);
        logOut.addActionListener(this::actionPerformed);
        userInfoMenu.add(logOut);

        mb.setLayout(new GridLayout(1, 12));
        mb.add(programMenu);
        mb.add(companiesMenu);

        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(new JLabel());
        mb.add(userInfoMenu);

        listModel = new CompaniesList(this);
        listScrollPane = new JScrollPane(listModel);
        listScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        companyContentPanel = new JPanel();
        companyContentPanel.setLayout(new BorderLayout());
        companyContentPanel.add(listScrollPane,BorderLayout.CENTER);
        filterPanel = new FilterPanel(this);
        companyContentPanel.add(filterPanel,BorderLayout.NORTH);

        this.add(companyContentPanel, BorderLayout.CENTER);

        showMyCompanies();

        profile = new PersonalPanel(me);
        this.add(profile,BorderLayout.EAST);
    }

    public void actionPerformed(ActionEvent e){
        if (e.getSource() == closeProgram)
            System.exit(0);
        if (e.getSource() == about){
            //todo show Integra logo and label
            aboutStuffPanel = new JPanel(new GridLayout(2,1));
            svgfile = new File("logo_trans.svg");
            try {
                iconsvg = new SVGIcon(svgfile.toURI().toString(),600,300);
            } catch (TranscoderException transcoderException) {
                transcoderException.printStackTrace();
            }
            aboutStuffPanel.add(new JLabel(new ImageIcon(iconsvg.getImage())));
        aboutStuffPanel.add(new JLabel("            Baza sponsorów Festiwalu Robotyki Robocomp"));
        JOptionPane.showMessageDialog(MainFrame.this, aboutStuffPanel, "O programie", JOptionPane.INFORMATION_MESSAGE);
    }
        else if (e.getSource() == logOut) {
            this.dispose();
            try {
                PrintWriter outputFile = new PrintWriter("adds/token");
                outputFile.print("");
                outputFile.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            EventQueue.invokeLater(LoginFrame::new);
        } else if (e.getSource() == addCompany) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new InputDataFrame(MainFrame.this).setVisible(true);
                }
            });
            showMyCompanies();
        } else if (e.getSource() == myCompanies) {
            showMyCompanies();
        } else if (e.getSource() == showProfile)
        {
            if(this.showProfile.getText().equals("Pokaż"))
            {
                profile = new PersonalPanel(ServerCommunication.queries.me());
                this.add(profile,BorderLayout.EAST);
                this.showProfile.setText("Ukryj");
            }
            else{
                this.showProfile.setText("Pokaż");
                this.remove(profile);
            }
            showMyCompanies();
        }
    }

    private void filterList()
    {
        Pattern pat = Pattern.compile(this.filterPanel.getFilterTextField(),Pattern.CASE_INSENSITIVE);
        CopyOnWriteArrayList<CompanyNode> new_array = new CopyOnWriteArrayList<>();
        if(this.filterPanel.getFilterComboBox().equals("Nazwa"))
        {
            for(CompanyNode node : downloadedResults) {
                if(pat.matcher(node.getName()).find())
                    new_array.add(node);
            }
            MainFrame.results = new_array;
        }
    }

    private void sortList(){
        String criterion = this.filterPanel.getSortComboBox();
        boolean asc = this.filterPanel.getSortTypeComboBox().equals("rosnąco");
        if(criterion.equals("Nazwa"))
            results.sort(new Comparator<CompanyNode>() {
                @Override
                public int compare(CompanyNode o1, CompanyNode o2) {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
        else if(criterion.equals("Osoba odpowiedzialna"))
            MainFrame.results.sort(new Comparator<CompanyNode>() {
                @Override
                public int compare(CompanyNode o1, CompanyNode o2) {
                    try{
                        String o1s = o1.getResponsiblePerson().getName()+o1.getResponsiblePerson().getSurname();
                        try{
                            String o2s = o2.getResponsiblePerson().getName()+o2.getResponsiblePerson().getSurname();
                            return o1s.compareToIgnoreCase(o2s);
                        }catch (NullPointerException a){
                            return 1;
                        }
                    }catch(NullPointerException a){
                        try{
                            String o2s = o2.getResponsiblePerson().getName()+o2.getResponsiblePerson().getSurname();
                            return -1;
                        }catch(NullPointerException b){
                            return 0;
                        }
                    }
                }
            });
        else if(criterion.equals("Data"))
            MainFrame.results.sort(new Comparator<CompanyNode>() {
                @Override
                public int compare(CompanyNode o1, CompanyNode o2) {
                    try{
                        String o1t = o1.getContactHistory().getEdges().get(o1.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                        try{
                            String o2t = o2.getContactHistory().getEdges().get(o2.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                            LocalDateTime c1t = LocalDateTime.parse(o1t);
                            LocalDateTime c2t = LocalDateTime.parse(o2t);
                            return c1t.compareTo(c2t);
                        }catch(IndexOutOfBoundsException|NullPointerException a){
                            return 1;
                        }
                    }catch(IndexOutOfBoundsException|NullPointerException a){
                        try{
                            String o2t = o2.getContactHistory().getEdges().get(o2.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                            return -1;
                        }catch(IndexOutOfBoundsException|NullPointerException e){
                            return 0;
                        }
                    }
                }
            });
        if(!asc)
            Collections.reverse(MainFrame.results);
    }

    public void showMyCompanies() {
        SwingWorker sw1 = new SwingWorker()
        {
            @Override
            protected String doInBackground() throws Exception {
                MainFrame.this.filterPanel.setElementsActive(false);
                MainFrame.this.listModel.loadingPanel();
                MainFrame.this.downloadedResults = ServerCommunication.queries.companiesAll();
                results = MainFrame.this.downloadedResults;
                filterAndShowList();
                MainFrame.this.filterPanel.setElementsActive(true);
                return "Companies downloaded";
            }
        };
        sw1.execute();
    }

    public void filterAndShowList() {
        this.filterList();
        this.sortList();
        showNewList();
    }

    public void showNewList() {
        this.listModel.displayNewList();
        this.paintComponents(getGraphics());
    }

    public CopyOnWriteArrayList<CompanyNode> getResults() {
        return results;
    }

    private JMenuBar mb = new JMenuBar();

    private JMenu programMenu = new JMenu("Program");

    private JMenuItem closeProgram = new JMenuItem("Wyjdź");
    private JMenuItem help = new JMenuItem("Pomoc");
    private JMenuItem about = new JMenuItem("O programie");

    private JMenu companiesMenu = new JMenu("Firmy");

    private JMenuItem findCompany = new JMenuItem("Znajdź firmę");
    private JMenuItem addCompany = new JMenuItem("Dodaj sponsora");
    private JMenuItem myCompanies = new JMenuItem("Pokaż aktualną listę firm");

    private JMenu staffMenu = new JMenu("Staff");

    private JMenuItem editUsers = new JMenuItem("Edit Users");

    private JMenu departmentsMenu = new JMenu("Departments");
    private JMenu historyMenu = new JMenu("Contact History");
    private JMenu summaryMenu = new JMenu("Cooperation Summary");

    private JMenu userInfoMenu = new JMenu("Mój profil");

    private JMenuItem showProfile = new JMenuItem("Ukryj");
    private JMenuItem logOut = new JMenuItem("Wyloguj mnie");

    private JPanel mainPanel = new JPanel();

    private CompaniesList listModel;
    private JScrollPane listScrollPane;

    private CopyOnWriteArrayList<CompanyNode> downloadedResults;
    private static CopyOnWriteArrayList<CompanyNode> results;

    private PersonalPanel profile;
    private StaffNode me;

    private JPanel companyContentPanel;
    private FilterPanel filterPanel;

    private JPanel aboutStuffPanel;

    private File svgfile;
    private SVGIcon iconsvg;

    private static final long serialVersionUID = 1L;
}
