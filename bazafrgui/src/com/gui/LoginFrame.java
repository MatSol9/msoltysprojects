package com.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.servercommunication.GlobalServerStuff;
import com.servercommunication.ServerCommunication;

/**
 * TODO
 * Metoda boolean checkUserInput(String login_, String password_) sprawdzająca czy dane użytkownika są poprawne
 * Zapisywanie danych użytkownika jeśli JCheckbox saveUserDataCheckbox jest zaznaczony
 */

public class LoginFrame extends JFrame implements KeyListener
{
    public LoginFrame()
    {
        super(GlobalOptions.superString);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);

        initGraphicalComponents();
        this.pack();

        this.setVisible(true);

        this.setIconImage(GlobalOptions.img.getImage());

        this.addKeyListener(this);
    }

    private void initGraphicalComponents()
    {
        this.add(mainPanel, BorderLayout.CENTER);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        //mainPanel.setBackground(GlobalOptions.buttonPanelBackgroundColor);

        mainPanel.add(buttonPanel, BorderLayout.SOUTH);
        //buttonPanel.setBackground(GlobalOptions.buttonPanelBackgroundColor);

        buttonPanel.add(loginButton);
        //loginButton.setBackground(GlobalOptions.buttonBackgroundColor);
       // loginButton.setForeground(GlobalOptions.buttonForegroundColor);
        loginButton.addActionListener(this::actionPerformed);
        loginButton.addKeyListener(this);

        inputUserData.setLayout(new BorderLayout());
        mainPanel.add(inputUserData, BorderLayout.NORTH);
        inputUserData.add(inputLoginPanel, BorderLayout.NORTH);
        //inputLoginPanel.setBackground(GlobalOptions.userInputColor);

        inputLoginPanel.add(userLogin);
        inputLoginPanel.add(inputUserLoginField);
        inputUserLoginField.addKeyListener(this);

        inputUserData.add(inputPasswordPanel, BorderLayout.CENTER);
        //inputPasswordPanel.setBackground(GlobalOptions.userInputColor);

        inputPasswordPanel.add(userPassword);
        inputPasswordPanel.add(inputUserPasswordField);
        inputUserPasswordField.addKeyListener(this);

        inputUserData.add(saveUserDataCheckbox, BorderLayout.SOUTH);
       // saveUserDataCheckbox.setBackground(GlobalOptions.userInputColor);
        saveUserDataCheckbox.addKeyListener(this);
        fillLoginDataFromSaved();
    }

    private void fillLoginDataFromSaved()
    {
        try
        {
            Scanner logData = new Scanner(new File("adds/token"));
            if(logData.hasNext())
            {
                inputUserLoginField.setText("(saved login)");
                inputUserPasswordField.setText("pswd");
                saveUserDataCheckbox.setSelected(true);
            }
        }
        catch(FileNotFoundException exc) {
            exc.printStackTrace();
        }
    }

    //Panele
    private JPanel mainPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();
    private JPanel inputLoginPanel = new JPanel();
    private JPanel inputPasswordPanel = new JPanel();
    private JPanel inputUserData = new JPanel();

    //Elementy
    private JLabel userLogin = new JLabel("Login:");
    private JTextField inputUserLoginField = new JTextField(15);

    private JLabel userPassword = new JLabel("Hasło:");
    private JPasswordField inputUserPasswordField = new JPasswordField(15);

    private JCheckBox saveUserDataCheckbox = new JCheckBox("Zapisz dane użytkownika");

    private JButton loginButton = new JButton("Zaloguj");

    private boolean ifLoggedIn;

    //Akcja logowania po wcisnieciu przycisku
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == loginButton) {
            loginProcess();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_ENTER)
            loginProcess();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    //Akcja logowania
    private void loginProcess()
    {
        String userLogin = inputUserLoginField.getText();
        String userPassword = new String (inputUserPasswordField.getPassword());
        checkUserInput(userLogin,userPassword);


    }

    private void loginThreadDone() {
        if(ifLoggedIn){
            EventQueue.invokeLater(() -> {
                new MainFrame(ServerCommunication.queries.me());
                dispose();
            });
        }
        else{
            EventQueue.invokeLater(WrongUserDataFrame::new);
            this.loginButton.setEnabled(true);
        }
    }

    private void checkUserInput(String login_, String password_){

        this.loginButton.setEnabled(false);
        SwingWorker sw1 = new SwingWorker()
        {
            @Override
            protected String doInBackground() throws Exception {
                if(password_.contains("\\") || login_.contains("\\")) {
                    LoginFrame.this.ifLoggedIn = false;
                    return "Not logged in";
                }
                LoginFrame.this.ifLoggedIn = ServerCommunication.queries.signIn(login_,password_,saveUserDataCheckbox.isSelected());
                return "Logged in";
            }

            @Override
            protected void done() {
                LoginFrame.this.loginThreadDone();
            }
        };
        sw1.execute();
    }
}
