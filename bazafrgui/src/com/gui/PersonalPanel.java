package com.gui;

import com.servercommunication.graphobjects.StaffNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class PersonalPanel extends JPanel {
    public PersonalPanel(StaffNode staffNode)
    {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        infoPanel = new JPanel();
        this.staffNode = staffNode;
        controlPanel = new JPanel();

        nameSurname = new JLabel(staffNode.getName() + " " + staffNode.getSurname());
        rank = new JLabel(String.valueOf(staffNode.getRank()));

        if(staffNode.getName().charAt(staffNode.getName().length() - 1) == 'a'){
            studyYearStart = new JLabel("Rozpoczęła studia w " + staffNode.getStudyYearStart());
        }
        else studyYearStart = new JLabel("Rozpoczął studia w " + staffNode.getStudyYearStart());

        email = new JLabel(staffNode.getEmail());

        try{
            department = new JLabel(staffNode.getDepartment().getName());
        }
        catch(NullPointerException a){
            department = new JLabel("Brak");
        }

        add(infoPanel);
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.PAGE_AXIS));

        infoPanel.add(new JLabel("Użytkownik:"));
        infoPanel.add(new JLabel("\n"));
        infoPanel.add(nameSurname);
        infoPanel.add(department);
        infoPanel.add(rank);
        infoPanel.add(email);
        infoPanel.add(studyYearStart);
        infoPanel.add(new JLabel("\n"));

        infoPanel.setAlignmentX(CENTER_ALIGNMENT);

        add(controlPanel);
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
        controlPanel.setAlignmentX(CENTER_ALIGNMENT);

        staffManagement = new JButton("Zarządzanie załogą");
        changeUserData = new JButton("Edytuj konto");
        passwordButton = new JButton("Zmień hasło");

        changeUserData.addActionListener(this::actionPerformed);
        passwordButton.addActionListener(this::actionPerformed);
        staffManagement.addActionListener(this::actionPerformed);

        controlPanel.add(changeUserData);
        controlPanel.add(passwordButton);

        if(staffNode.getRank() == GlobalOptions.RANKS.ADMIN && false || staffNode.getRank() == GlobalOptions.RANKS.MAIN_ORGANIZER && false){
            controlPanel.add(staffManagement);
        }
    }

    private JLabel nameSurname;

    private JPanel infoPanel;
    private JPanel controlPanel;

    private JLabel studyYearStart;
    private JLabel department;
    private JLabel rank;
    private JLabel email;

    private JButton staffManagement;
    private JButton changeUserData;
    private JButton passwordButton;

    private StaffNode staffNode;

    private void actionPerformed(ActionEvent e){
        if(e.getSource() == changeUserData){
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new MyAccountInfoFrame(staffNode).setVisible(true);
                }
            });
        }
        else if(e.getSource() == passwordButton){
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new ChangePasswordFrame(staffNode).setVisible(true);
                }
            });
        }
        else if(e.getSource() == staffManagement){
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new StaffGovFrame(staffNode).setVisible(true);
                }
            });
        }
    }
}
