package com.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;


public class FilterPanel extends JPanel {

    public FilterPanel(MainFrame mainFrame)
    {
        this.setLayout(new GridLayout(3,9));
        initGraphicalComponents();

        this.mainFrame = mainFrame;
    }

    public void initGraphicalComponents(){
        sortComboBox = new JComboBox();
        sortComboBox.addItem("Nazwa");
        sortComboBox.addItem("Osoba odpowiedzialna");
        sortComboBox.addItem("Data");
        sortTypeComboBox = new JComboBox();
        sortTypeComboBox.addItem("rosnąco");
        sortTypeComboBox.addItem("malejąco");

        filterButton = new JButton("Filtruj");
        filterButton.addActionListener(this::actionPerformed);
        filterComboBox = new JComboBox();
        filterComboBox.addItem("Nazwa");

        filterTextField = new JTextField(20);
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel("Kategoria"));
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel("Kategoria"));
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(sortComboBox);
        this.add(sortTypeComboBox);
        this.add(new JLabel());
        this.add(filterTextField);
        this.add(filterComboBox);
        this.add(filterButton);
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
    }

    private void actionPerformed(ActionEvent e) {
        if(e.getSource() == filterButton)
        {
            mainFrame.filterAndShowList();
        }
    }

    public String getFilterTextField() {
        return filterTextField.getText();
    }

    public String getFilterComboBox() {
        return filterComboBox.getSelectedItem().toString();
    }

    public String getSortComboBox() {
        return sortComboBox.getSelectedItem().toString();
    }

    public String getSortTypeComboBox() {
        return sortTypeComboBox.getSelectedItem().toString();
    }

    public void setElementsActive(boolean ifActive) {
        this.sortComboBox.setEnabled(ifActive);
        this.sortTypeComboBox.setEnabled(ifActive);
        this.filterButton.setEnabled(ifActive);
        this.filterComboBox.setEnabled(ifActive);
        this.filterTextField.setEnabled(ifActive);
    }

    private JComboBox sortComboBox;
    private JComboBox sortTypeComboBox;

    private JButton filterButton;
    private JComboBox filterComboBox;
    private JTextField filterTextField;

    private MainFrame mainFrame;
}
