package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.ContactHistoryNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

public class ContactHistoryPanel extends JPanel {
    public ContactHistoryPanel(ContactHistoryNode contactHistoryNode, InputDataFrame inputDataFrame){
        this.contactHistoryNode = contactHistoryNode;
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        String dateString = this.contactHistoryNode.getContactDate();
        if(LocalDateTime.parse(dateString).getMinute() < 10){
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":0" + LocalDateTime.parse(dateString).getMinute();
        }
        else{
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":" + LocalDateTime.parse(dateString).getMinute();
        }

        this.dateField.setText(dateString);

        String tempNote;
        try {
            tempNote = URLDecoder.decode(contactHistoryNode.getNotes(), StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
        tempNote = tempNote.split("\n")[0];
        if(tempNote.length() > GlobalOptions.notePanelNoteLength){
            tempNote = tempNote.substring(0, GlobalOptions.notePanelNoteLength-4) + "...";
        }
        this.notesArea.setText(tempNote);
        this.notesArea.setLineWrap(true);
        this.dateField.setEditable(false);
        this.notesArea.setEditable(false);

        initGraphicalComponents();


        this.parentFrame = inputDataFrame;
    }

    private void initGraphicalComponents(){
        this.add(datePanel, BorderLayout.NORTH);
        datePanel.setLayout(new BorderLayout());
        datePanel.add(dateLabel, BorderLayout.NORTH);
        datePanel.add(dateField, BorderLayout.SOUTH);

        this.add(notesPanel, BorderLayout.CENTER);
        notesPanel.setLayout(new BorderLayout());
        notesPanel.add(notesLabel, BorderLayout.NORTH);
        notesPanel.add(notesArea, BorderLayout.SOUTH);

        this.add(buttonPanel, BorderLayout.SOUTH);
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.add(editButton);
        editButton.addActionListener(this::actionPerformed);
        buttonPanel.add(deleteButton);
        deleteButton.addActionListener(this::actionPerformed);
    }

    private JPanel datePanel = new JPanel();
    private JLabel dateLabel = new JLabel("Data:");
    private JFormattedTextField dateField = new JFormattedTextField();

    private JPanel notesPanel = new JPanel();
    private JLabel notesLabel = new JLabel("Notatki:");
    private JTextArea notesArea = new JTextArea();

    private JPanel buttonPanel = new JPanel();
    private JButton editButton = new JButton("Szczegóły");
    private JButton deleteButton = new JButton("Usuń");

    private ContactHistoryNode contactHistoryNode;

    private InputDataFrame parentFrame;

    public void actionPerformed(ActionEvent e){
        if(e.getSource() == this.deleteButton){
            ServerCommunication.mutations.contactHistoryDelete(this.contactHistoryNode.getId());
            parentFrame.showNotes();
        }
        if(e.getSource() == this.editButton){
            new NoteFrame(contactHistoryNode, parentFrame.getId()).setVisible(true);
        }
    }
}
