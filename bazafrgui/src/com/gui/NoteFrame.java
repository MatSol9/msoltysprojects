package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.ContactHistoryNode;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDateTime;

public class NoteFrame extends JFrame {
    public NoteFrame(String companyId){
        super("Nowa notatka z kontaktu");
        initFrame();
        this.dateTextField.setEditable(false);
        String dateString = String.valueOf(java.time.LocalDateTime.now());
        if(LocalDateTime.parse(dateString).getMinute() < 10){
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":0" + LocalDateTime.parse(dateString).getMinute();
        }
        else{
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":" + LocalDateTime.parse(dateString).getMinute();
        }
        this.dateTextField.setText(dateString);
        this.pack();
        this.companyId = companyId;
        this.setIconImage(GlobalOptions.img.getImage());
    }

    public NoteFrame(ContactHistoryNode contactHistoryNode, String companyId){
        super("Notatka z kontaktu");
        initFrame();
        this.editButton = new JButton("Edytuj");
        this.editButton.addActionListener(this::actionPerformed);
        this.buttonPanel.add(editButton);
        this.saveButton.setEnabled(false);
        this.pack();
        this.setIconImage(GlobalOptions.img.getImage());

        String dateString = contactHistoryNode.getContactDate();
        if(LocalDateTime.parse(dateString).getMinute() < 10){
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":0" + LocalDateTime.parse(dateString).getMinute();
        }
        else{
            dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":" + LocalDateTime.parse(dateString).getMinute();
        }
        this.dateTextField.setText(dateString);

        try {
            this.noteArea.setText(URLDecoder.decode(contactHistoryNode.getNotes(), StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }

        this.id = contactHistoryNode.getId();
        this.companyId = companyId;
        this.dateTextField.setEditable(false);
        this.noteArea.setEditable(false);
    }

    public void initFrame(){
        this.setLayout(new FlowLayout());
        this.setLocationRelativeTo(null);
        initGraphicalComponents();
        this.requestFocus();
        this.setResizable(false);
    }

    private void initGraphicalComponents(){
        mainPanel = new JPanel();
        datePanel = new JPanel();
        buttonPanel = new JPanel();
        notePanel = new JPanel();

        dateTextField = new JTextField();
        noteArea = new JTextArea(10, 40);
        noteArea.setLineWrap(true);

        this.add(this.mainPanel);
        this.mainPanel.setLayout(new BorderLayout());
        this.mainPanel.add(this.datePanel, BorderLayout.NORTH);

        this.datePanel.setLayout(new BorderLayout());
        this.datePanel.add(new JLabel("Data"), BorderLayout.NORTH);
        this.datePanel.add(this.dateTextField, BorderLayout.CENTER);

        this.mainPanel.add(this.notePanel, BorderLayout.CENTER);
        this.notePanel.setLayout(new BorderLayout());
        this.notePanel.add(new JLabel("Treść:"), BorderLayout.NORTH);
        this.notePanel.add(noteArea, BorderLayout.CENTER);
        this.noteArea.setLineWrap(true);

        this.mainPanel.add(this.buttonPanel, BorderLayout.SOUTH);
        this.buttonPanel.setLayout(new GridLayout());
        this.cancelButton = new JButton("Anuluj");
        this.saveButton = new JButton("Zapisz");
        this.buttonPanel.add(saveButton);
        this.buttonPanel.add(cancelButton);
        cancelButton.addActionListener(this::actionPerformed);
        saveButton.addActionListener(this::actionPerformed);
    }

    private JTextField dateTextField;
    private JTextArea noteArea;

    private JPanel mainPanel;
    private JPanel datePanel;
    private JPanel buttonPanel;
    private JPanel notePanel;

    private JButton saveButton;
    private JButton cancelButton;
    private JButton editButton;

    private String id;
    private String companyId;

    private void actionPerformed(ActionEvent e){
        if(id != null){
            if(e.getSource() == cancelButton){
                dispose();
            }
            if(e.getSource() == editButton){
                this.noteArea.setEditable(true);
                this.editButton.setEnabled(false);
                this.saveButton.setEnabled(true);
            }
            if(e.getSource() == saveButton){
                try {
                    ServerCommunication.mutations.contactHistoryUpdate(String.valueOf(java.time.LocalDateTime.now()), this.id, URLEncoder.encode(this.noteArea.getText(), StandardCharsets.UTF_8.toString()));
                    dispose();
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException(ex.getCause());
                }
            }
        }
        else{
            if(e.getSource() == cancelButton){
                dispose();
            }
            if(e.getSource() == saveButton){
                try {
                    ServerCommunication.mutations.contactHistoryCreate(companyId,String.valueOf(java.time.LocalDateTime.now()),URLEncoder.encode(this.noteArea.getText(), StandardCharsets.UTF_8.toString()));
                    dispose();
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException(ex.getCause());
                }

            }
        }
    }
}
