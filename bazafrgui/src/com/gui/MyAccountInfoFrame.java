package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.StaffNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class MyAccountInfoFrame extends JFrame {
    MyAccountInfoFrame(StaffNode staffNode){
        super("Edytuj informacje o koncie");
        this.me = staffNode;
        this.setIconImage(GlobalOptions.img.getImage());
        initGraphicalComponents();
        this.nameField.setText(this.me.getName());
        this.surnameField.setText(this.me.getSurname());
        this.emailField.setText(this.me.getEmail());
        this.studyYearField.setText(String.valueOf(this.me.getStudyYearStart()));
        this.pack();
    }

    private void initGraphicalComponents(){
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.PAGE_AXIS));

        this.namePanel = new JPanel();
        this.mainPanel.add(this.namePanel);
        this.nameField = new JTextField();
        this.namePanel.setLayout(new BoxLayout(this.namePanel, BoxLayout.PAGE_AXIS));
        this.namePanel.add(new JLabel("Imię"));
        this.namePanel.add(this.nameField);

        this.surnamePanel = new JPanel();
        this.mainPanel.add(this.surnamePanel);
        this.surnameField = new JTextField();
        this.surnamePanel.setLayout(new BoxLayout(this.surnamePanel, BoxLayout.PAGE_AXIS));
        this.surnamePanel.add(new JLabel("Nazwisko"));
        this.surnamePanel.add(this.surnameField);

        this.emailPanel = new JPanel();
        this.mainPanel.add(this.emailPanel);
        this.emailField = new JTextField();
        this.emailPanel.setLayout(new BoxLayout(this.emailPanel, BoxLayout.PAGE_AXIS));
        this.emailPanel.add(new JLabel("e-mail"));
        this.emailPanel.add(this.emailField);

        this.studyYearPanel = new JPanel();
        this.mainPanel.add(this.studyYearPanel);
        this.studyYearField = new JTextField();
        this.studyYearPanel.setLayout(new BoxLayout(this.studyYearPanel, BoxLayout.PAGE_AXIS));
        this.studyYearPanel.add(new JLabel("Rok rozpoczęcia studiów"));
        this.studyYearPanel.add(this.studyYearField);

        this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout(new GridLayout());
        this.mainPanel.add(this.buttonPanel);
        this.saveButton = new JButton("Zapisz");
        this.cancelButton = new JButton("Anuluj");
        this.saveButton.addActionListener(this::actionPerformed);
        this.cancelButton.addActionListener(this::actionPerformed);
        this.buttonPanel.add(this.saveButton);
        this.buttonPanel.add(this.cancelButton);

        this.add(this.mainPanel);
    }

    private StaffNode me;

    private JPanel mainPanel;

    private JPanel namePanel;
    private JPanel surnamePanel;
    private JPanel emailPanel;
    private JPanel studyYearPanel;
    private JPanel buttonPanel;

    private JTextField nameField;
    private JTextField surnameField;
    private JTextField emailField;
    private JTextField studyYearField;

    private JButton saveButton;
    private JButton cancelButton;

    private void actionPerformed(ActionEvent e){
        if(e.getSource() == cancelButton){
            dispose();
        }
        else if(e.getSource() == saveButton){
            ServerCommunication.mutations.staffUpdate(this.me.getId(), this.nameField.getText(), this.surnameField.getText(), this.emailField.getText(), Integer.parseInt(this.studyYearField.getText()));
            dispose();
        }

    }
}
