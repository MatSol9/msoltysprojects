package com.servercommunication;

import java.util.HashMap;
import java.util.Map;

public class GlobalServerStuff {
    public static String serverURL = "Placeholder";
    public static Map<String ,String> headers;
    public static ServerCommunication communication = new ServerCommunication();
    static {
        GlobalServerStuff.headers = new HashMap<>();
    }

    public static void setMyToken(String myToken) {
        GlobalServerStuff.headers.put("Authorization", "Bearer " + myToken);
    }

    public static Map<String,String> getHeaders(){
        return GlobalServerStuff.headers;
    }
}
