package com.servercommunication.filterobjects;

public class StaffFilter {
    private String name;
    private String nameNe;
    private String nameIn;
    private String nameLike;
    private String nameIlike;
    
    private String surname; 
    private String surnameNe; 
    private String surnameIn;
    private String surnameLike; 
    private String surnameIlike; 
    
    private Boolean active;
    private Boolean activeNe;
    
    private StaffFilter[] and;
    private StaffFilter[] or;
    private StaffFilter not;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameNe() {
        return nameNe;
    }

    public void setNameNe(String nameNe) {
        this.nameNe = nameNe;
    }

    public String getNameIn() {
        return nameIn;
    }

    public void setNameIn(String nameIn) {
        this.nameIn = nameIn;
    }

    public String getNameLike() {
        return nameLike;
    }

    public void setNameLike(String nameLike) {
        this.nameLike = nameLike;
    }

    public String getNameIlike() {
        return nameIlike;
    }

    public void setNameIlike(String nameIlike) {
        this.nameIlike = nameIlike;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurnameNe() {
        return surnameNe;
    }

    public void setSurnameNe(String surnameNe) {
        this.surnameNe = surnameNe;
    }

    public String getSurnameIn() {
        return surnameIn;
    }

    public void setSurnameIn(String surnameIn) {
        this.surnameIn = surnameIn;
    }

    public String getSurnameLike() {
        return surnameLike;
    }

    public void setSurnameLike(String surnameLike) {
        this.surnameLike = surnameLike;
    }

    public String getSurnameIlike() {
        return surnameIlike;
    }

    public void setSurnameIlike(String surnameIlike) {
        this.surnameIlike = surnameIlike;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getActiveNe() {
        return activeNe;
    }

    public void setActiveNe(Boolean activeNe) {
        this.activeNe = activeNe;
    }

    public StaffFilter[] getAnd() {
        return and;
    }

    public void setAnd(StaffFilter[] and) {
        this.and = and;
    }

    public StaffFilter[] getOr() {
        return or;
    }

    public void setOr(StaffFilter[] or) {
        this.or = or;
    }

    public StaffFilter getNot() {
        return not;
    }

    public void setNot(StaffFilter not) {
        this.not = not;
    }
}
