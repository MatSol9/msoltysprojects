package com.servercommunication.filterobjects;

public class CooperationSummaryFilter {
    private String year;
    private String yearNe;
    private String yearLt;
    private String yearLte;
    private String yearGt;
    private String yearGte;

    private String yearRange;

    private CooperationSummaryFilter[] and;
    private CooperationSummaryFilter[] or;
    private CooperationSummaryFilter not;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYearNe() {
        return yearNe;
    }

    public void setYearNe(String yearNe) {
        this.yearNe = yearNe;
    }

    public String getYearLt() {
        return yearLt;
    }

    public void setYearLt(String yearLt) {
        this.yearLt = yearLt;
    }

    public String getYearLte() {
        return yearLte;
    }

    public void setYearLte(String yearLte) {
        this.yearLte = yearLte;
    }

    public String getYearGt() {
        return yearGt;
    }

    public void setYearGt(String yearGt) {
        this.yearGt = yearGt;
    }

    public String getYearGte() {
        return yearGte;
    }

    public void setYearGte(String yearGte) {
        this.yearGte = yearGte;
    }

    public String getYearRange() {
        return yearRange;
    }

    public void setYearRange(String yearRange) {
        this.yearRange = yearRange;
    }

    public CooperationSummaryFilter[] getAnd() {
        return and;
    }

    public void setAnd(CooperationSummaryFilter[] and) {
        this.and = and;
    }

    public CooperationSummaryFilter[] getOr() {
        return or;
    }

    public void setOr(CooperationSummaryFilter[] or) {
        this.or = or;
    }

    public CooperationSummaryFilter getNot() {
        return not;
    }

    public void setNot(CooperationSummaryFilter not) {
        this.not = not;
    }
}
