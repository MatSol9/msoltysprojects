package com.servercommunication.filterobjects;

public class CompanyFilter {
    private String name;
    private String nameNe;
    private String nameIn;
    private String nameLike;
    private String nameIlike;

    private String email;
    private String emailNe;
    private String emailIn;
    private String emailLike;
    private String emailIlike;

    private String www;
    private String wwwNe;
    private String wwwIn;
    private String wwwLike;
    private String wwwIlike;

    private String phoneNumber;
    private String phoneNumberNe;
    private String phoneNumberIn;
    private String phoneNumberLike;
    private String phoneNumberIlike;

    private int potential;
    private int potentialLt;
    private int potentialLte;
    private int potentialGt;
    private int potentialGte;
    private int potentialNe;
    private int potentialIn;
    private int potentialNotIn;
    private int potentialRange;

    private CompanyFilter[] and;
    private CompanyFilter[] or;
    private CompanyFilter not;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameNe() {
        return nameNe;
    }

    public void setNameNe(String nameNe) {
        this.nameNe = nameNe;
    }

    public String getNameIn() {
        return nameIn;
    }

    public void setNameIn(String nameIn) {
        this.nameIn = nameIn;
    }

    public String getNameLike() {
        return nameLike;
    }

    public void setNameLike(String nameLike) {
        this.nameLike = nameLike;
    }

    public String getNameIlike() {
        return nameIlike;
    }

    public void setNameIlike(String nameIlike) {
        this.nameIlike = nameIlike;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailNe() {
        return emailNe;
    }

    public void setEmailNe(String emailNe) {
        this.emailNe = emailNe;
    }

    public String getEmailIn() {
        return emailIn;
    }

    public void setEmailIn(String emailIn) {
        this.emailIn = emailIn;
    }

    public String getEmailLike() {
        return emailLike;
    }

    public void setEmailLike(String emailLike) {
        this.emailLike = emailLike;
    }

    public String getEmailIlike() {
        return emailIlike;
    }

    public void setEmailIlike(String emailIlike) {
        this.emailIlike = emailIlike;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getWwwNe() {
        return wwwNe;
    }

    public void setWwwNe(String wwwNe) {
        this.wwwNe = wwwNe;
    }

    public String getWwwIn() {
        return wwwIn;
    }

    public void setWwwIn(String wwwIn) {
        this.wwwIn = wwwIn;
    }

    public String getWwwLike() {
        return wwwLike;
    }

    public void setWwwLike(String wwwLike) {
        this.wwwLike = wwwLike;
    }

    public String getWwwIlike() {
        return wwwIlike;
    }

    public void setWwwIlike(String wwwIlike) {
        this.wwwIlike = wwwIlike;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumberNe() {
        return phoneNumberNe;
    }

    public void setPhoneNumberNe(String phoneNumberNe) {
        this.phoneNumberNe = phoneNumberNe;
    }

    public String getPhoneNumberIn() {
        return phoneNumberIn;
    }

    public void setPhoneNumberIn(String phoneNumberIn) {
        this.phoneNumberIn = phoneNumberIn;
    }

    public String getPhoneNumberLike() {
        return phoneNumberLike;
    }

    public void setPhoneNumberLike(String phoneNumberLike) {
        this.phoneNumberLike = phoneNumberLike;
    }

    public String getPhoneNumberIlike() {
        return phoneNumberIlike;
    }

    public void setPhoneNumberIlike(String phoneNumberIlike) {
        this.phoneNumberIlike = phoneNumberIlike;
    }

    public int getPotential() {
        return potential;
    }

    public void setPotential(int potential) {
        this.potential = potential;
    }

    public int getPotentialLt() {
        return potentialLt;
    }

    public void setPotentialLt(int potentialLt) {
        this.potentialLt = potentialLt;
    }

    public int getPotentialLte() {
        return potentialLte;
    }

    public void setPotentialLte(int potentialLte) {
        this.potentialLte = potentialLte;
    }

    public int getPotentialGt() {
        return potentialGt;
    }

    public void setPotentialGt(int potentialGt) {
        this.potentialGt = potentialGt;
    }

    public int getPotentialGte() {
        return potentialGte;
    }

    public void setPotentialGte(int potentialGte) {
        this.potentialGte = potentialGte;
    }

    public int getPotentialNe() {
        return potentialNe;
    }

    public void setPotentialNe(int potentialNe) {
        this.potentialNe = potentialNe;
    }

    public int getPotentialIn() {
        return potentialIn;
    }

    public void setPotentialIn(int potentialIn) {
        this.potentialIn = potentialIn;
    }

    public int getPotentialNotIn() {
        return potentialNotIn;
    }

    public void setPotentialNotIn(int potentialNotIn) {
        this.potentialNotIn = potentialNotIn;
    }

    public int getPotentialRange() {
        return potentialRange;
    }

    public void setPotentialRange(int potentialRange) {
        this.potentialRange = potentialRange;
    }

    public CompanyFilter[] getAnd() {
        return and;
    }

    public void setAnd(CompanyFilter[] and) {
        this.and = and;
    }

    public CompanyFilter[] getOr() {
        return or;
    }

    public void setOr(CompanyFilter[] or) {
        this.or = or;
    }

    public CompanyFilter getNot() {
        return not;
    }

    public void setNot(CompanyFilter not) {
        this.not = not;
    }
}
