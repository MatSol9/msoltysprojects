package com.servercommunication;

import com.servercommunication.graphobjects.*;
import com.servercommunication.queryobjects.*;
import io.aexp.nodes.graphql.Argument;
import io.aexp.nodes.graphql.Arguments;
import io.aexp.nodes.graphql.GraphQLResponseEntity;

import java.io.PrintWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

public class Queries {

    /**
     * Funkcja zwracająca informacie o zalogowanym użytkowniku
     * @return obiekt klasy StaffNode zawierający dane użytkownika
     * @throws NullPointerException rzuca kiedy coś się schrzani
     */
    public StaffNode me() throws NullPointerException {
        QMSender<Me> qmSender = new QMSender<>(Me.class);
        return qmSender.getResponse().getResponse();
    }

    /**
     * Funkcja logująca
     * @param username login użytkownika
     * @param password hasło użytkownika
     * @param rememberMe czy zapamiętać
     * @return true jeśli zaloguje, false jeśli nie
     */
    public boolean signIn(String username, String password, boolean rememberMe){

        QMSender<SignIn> qmSender = new QMSender<>(SignIn.class, new Arguments(
                "signIn",
                new Argument("username", username),
                new Argument("password", password),
                new Argument("generateToken", true),
                new Argument("rememberMe", rememberMe)),
                false,
                false
        );
        try{
            String loginToken = qmSender.getResponse().getResponse().getAuthToken();
            GlobalServerStuff.setMyToken(loginToken);
            try {
                PrintWriter outputFile = new PrintWriter("adds/token");
                outputFile.println(rememberMe ? GlobalServerStuff.getHeaders().get("Authorization") : "");
                outputFile.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        catch(NullPointerException a) {
            //Zwraca false jak wyjebie nullpointer (czyli złe dane zapytania)
            return false;
        }return true;
    }


    /**
     * Funkcja zwracająca wszystkie firmy
     * @return arraylista obiektów CompanyNode przechowujących dane firmy
     * @throws NullPointerException jeśli coś się schrzani
     */
    public CopyOnWriteArrayList<CompanyNode> companiesAll() throws NullPointerException{
        CopyOnWriteArrayList<CompanyNode> result = new CopyOnWriteArrayList<>();
        ArrayList<CompanyNodeEdge> companyNodeConnection;
        QMSender<CompaniesAll> qmSender = new QMSender<>(CompaniesAll.class);
        GraphQLResponseEntity<CompaniesAll> responseEntity = qmSender.getResponse();
        companyNodeConnection = responseEntity.getResponse().getEdges();
        for(CompanyNodeEdge elem: companyNodeConnection){result.add(elem.getNode());}
        return result;
    }

    /**
     * Funkcja zwracająca historię komunikacji z daną firmą
     * @param companyId ID firmy której historię komunikacji chcemy obejrzeć
     * @return Arraylista obiektów ContactHistoryNode które zawierają poszczególne wpisy w historii
     * @throws NullPointerException jeśli coś się schrzani
     */
    public ArrayList<ContactHistoryNode> contactHistoryByCompany(String companyId) throws NullPointerException{
        ArrayList<ContactHistoryNode> result = new ArrayList<>();
        ArrayList<ContactHistoryNodeEdge> contactNodeConnection;
        QMSender<ContactHistoryByCompany> qmSender = new QMSender<>(ContactHistoryByCompany.class,
                new Arguments(
                        "contactHistoryByCompany",
                        new Argument("companyId", companyId)
                ),
                true,
                false);
        contactNodeConnection = qmSender.getResponse().getResponse().getEdges();
        for(ContactHistoryNodeEdge elem: contactNodeConnection) result.add(elem.getNode());
        return result;
    }

    /**
     * Funkcja zwracająca podsumowanie współpracy z daną firmą
     * @param companyId ID firmy
     * @return arraylista obiektów CooperationSummaryNode zawierających podsumowania współpracy
     * @throws NullPointerException jeśli coś się schrzani
     */
    public ArrayList<CooperationSummaryNode> cooperationSummaryByCompany(String companyId) throws NullPointerException{
        ArrayList<CooperationSummaryNode> result = new ArrayList<>();
        ArrayList<CooperationSummaryNodeEdge> cooperationSummaryNodeConnection;
        QMSender<CooperationSummaryByCompany> qmSender = new QMSender<>(CooperationSummaryByCompany.class,
                new Arguments(
                        "cooperationSummaryByCompany",
                        new Argument("companyId", companyId)
                ),
                true,
                false);
        cooperationSummaryNodeConnection = qmSender.getResponse().getResponse().getEdges();
        for(CooperationSummaryNodeEdge elem: cooperationSummaryNodeConnection) result.add(elem.getNode());
        return result;
    }

    /**
     * Funkcja zwracająca podsumowania współpracy w danym roku
     * @param year rok
     * @return arraylista obiektów CooperationSummaryNode zawierających podsumowania współpracy
     * @throws NullPointerException jeśli coś się schrzani
     */
    ArrayList<CooperationSummaryNode> sendCooperationSummaryByYear(String year) throws NullPointerException{
        ArrayList<CooperationSummaryNode> result = new ArrayList<>();
        ArrayList<CooperationSummaryNodeEdge> cooperationSummaryNodeConnection;
        QMSender<CooperationSummaryByYear> qmSender = new QMSender<>(CooperationSummaryByYear.class,
                new Arguments(
                        "cooperationSummaryByYear",
                        new Argument("year", year)
                ),
                true,
                false);
        cooperationSummaryNodeConnection = qmSender.getResponse().getResponse().getEdges();
        for(CooperationSummaryNodeEdge elem: cooperationSummaryNodeConnection) result.add(elem.getNode());
        return result;
    }

    /**
     * Funkcja zwracająca wszystkie działy
     * @return arraylista obiektów DepartmentNode zawierających poszczególne działy
     * @throws NullPointerException jeśli coś się schrzani
     */
    public ArrayList<DepartmentNode> departmentsAll() throws NullPointerException{
        ArrayList<DepartmentNode> result = new ArrayList<>();
        ArrayList<DepartmentNodeEdge> companyNodeConnection;
        QMSender<DepartmentsAll> qmSender = new QMSender<>(DepartmentsAll.class);
        companyNodeConnection = qmSender.getResponse().getResponse().getEdges();
        for(DepartmentNodeEdge elem: companyNodeConnection) result.add(elem.getNode());
        return result;
    }

    /**
     * Funkcja zwracająca wszystkich ludzi
     * @return arraylista obiektów StaffNode zawierających członków załogi
     * @throws NullPointerException jeśli coś się schrzani
     */
    public ArrayList<StaffNode> staffersAll() throws NullPointerException{
        ArrayList<StaffNode> result = new ArrayList<>();
        ArrayList<StaffNodeEdge> staffNodeConnection;
        QMSender<StaffersAll> qmSender = new QMSender<>(StaffersAll.class);
        staffNodeConnection = qmSender.getResponse().getResponse().getEdges();
        for(StaffNodeEdge elem: staffNodeConnection) result.add(elem.getNode());
        return result;
    }

    public String companyGetContactDate(String companyId) throws NullPointerException{
        ArrayList<ContactHistoryNode> histories = ServerCommunication.queries.contactHistoryByCompany(companyId);
        ArrayList<java.time.LocalDateTime> contactDates = new ArrayList<>();
        for(ContactHistoryNode elem : histories){
            contactDates.add(LocalDateTime.parse(elem.getContactDate()));
        }
        if(contactDates.isEmpty()) throw new NullPointerException("Brak kontaktu");

        LocalDateTime resultTime = contactDates.get(0);
        for(LocalDateTime elem : contactDates){
            if(elem.isAfter(resultTime)) resultTime = elem;
        }
        return resultTime.toString();
    }
}
