package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.ContactPersonNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="companyContactPersonDelete",
        arguments={
                @GraphQLArgument(name="contactPersonId")
        }
)
public class CompanyContactPersonDelete {
    private ContactPersonNode contactPersonNode;

    public ContactPersonNode getContactPersonNode() {
        return contactPersonNode;
    }

    public void setContactPersonNode(ContactPersonNode contactPersonNode) {
        this.contactPersonNode = contactPersonNode;
    }
}
