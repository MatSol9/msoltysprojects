package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.StaffNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;


@GraphQLProperty(
        name = "staffChangeDepartment",
        arguments = {
                @GraphQLArgument(name = "department"),
                @GraphQLArgument(name = "staffId")
        }
)
public class StaffChangeDepartment {
    private StaffNode staff;

    public StaffNode getStaff() {
        return staff;
    }

    public void setStaff(StaffNode staff) {
        this.staff = staff;
    }
}
