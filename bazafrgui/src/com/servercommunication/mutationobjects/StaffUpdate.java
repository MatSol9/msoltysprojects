package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.StaffNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="staffUpdate",
        arguments = {
                @GraphQLArgument(name = "email"),
                @GraphQLArgument(name = "name"),
                @GraphQLArgument(name = "studyYearStart", type="int"),
                @GraphQLArgument(name = "surname"),
                @GraphQLArgument(name = "staffId")
        }
)
public class StaffUpdate {
    private StaffNode staff;

    public StaffNode getStaff() {
        return staff;
    }

    public void setStaff(StaffNode staff) {
        this.staff = staff;
    }
}
