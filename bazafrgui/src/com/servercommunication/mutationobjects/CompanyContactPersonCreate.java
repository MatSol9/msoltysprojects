package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.ContactPersonNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="companyContactPersonCreate",
        arguments={
                @GraphQLArgument(name="companyId"),
                @GraphQLArgument(name="email"),
                @GraphQLArgument(name="name"),
                @GraphQLArgument(name="notes"),
                @GraphQLArgument(name="phoneNumber")
        }
)
public class CompanyContactPersonCreate {
    private ContactPersonNode contactPersonNode;

    public ContactPersonNode getContactPersonNode() {
        return contactPersonNode;
    }

    public void setContactPersonNode(ContactPersonNode contactPersonNode) {
        this.contactPersonNode = contactPersonNode;
    }
}
