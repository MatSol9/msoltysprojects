package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.StaffNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="staffChangePassword",
        arguments = {
                @GraphQLArgument(name = "newPassword"),
                @GraphQLArgument(name = "oldPassword"),
                @GraphQLArgument(name = "staffId")
        }
)
public class StaffChangePassword {
    private StaffNode staff;

    public StaffNode getStaff() {
        return staff;
    }

    public void setStaff(StaffNode staff) {
        this.staff = staff;
    }
}
