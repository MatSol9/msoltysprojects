package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.StaffNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="staffCreate",
        arguments = {
                @GraphQLArgument(name = "department"),
                @GraphQLArgument(name = "email"),
                @GraphQLArgument(name = "name"),
                @GraphQLArgument(name = "password"),
                @GraphQLArgument(name = "rank"),
                @GraphQLArgument(name = "studyYearStart", type="int"),
                @GraphQLArgument(name = "surname"),
                @GraphQLArgument(name = "username")
        }
)
public class StaffCreate {
    private StaffNode staff;

    public StaffNode getStaff() {
        return staff;
    }

    public void setStaff(StaffNode staff) {
        this.staff = staff;
    }
}
