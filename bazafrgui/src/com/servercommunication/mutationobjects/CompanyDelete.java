package com.servercommunication.mutationobjects;

import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="companyDelete",
        arguments = {
                @GraphQLArgument(name="companyId")
        }
)
public class CompanyDelete {
    private String companyId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
