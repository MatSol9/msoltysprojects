package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.CooperationSummaryNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;


@GraphQLProperty(
        name = "cooperationSummaryUpdate",
        arguments = {
                @GraphQLArgument(name = "cooperationSummaryId"),
                @GraphQLArgument(name = "results"),
                @GraphQLArgument(name = "summary")
        }
)
public class CooperationSummaryUpdate {
    private CooperationSummaryNode cooperationSummary;

    public CooperationSummaryNode getCooperationSummary() {
        return cooperationSummary;
    }

    public void setCooperationSummary(CooperationSummaryNode cooperationSummary) {
        this.cooperationSummary = cooperationSummary;
    }
}
