package com.servercommunication.mutationobjects;


import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name = "contactHistoryDelete",
        arguments = {
                @GraphQLArgument(name = "contactHistoryId")
        }
)
public class ContactHistoryDelete {
    private String contactHistoryId;

    public String getContactHistoryId() {
        return contactHistoryId;
    }

    public void setContactHistoryId(String contactHistoryId) {
        this.contactHistoryId = contactHistoryId;
    }
}
