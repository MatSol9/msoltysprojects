package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.CompanyNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name="companySetResponsiblePerson",
        arguments = {
                @GraphQLArgument(name = "companyId"),
                @GraphQLArgument(name = "staffId")
        }
)
public class CompanySetResponsiblePerson {
    private CompanyNode company;

    public CompanyNode getCompany() {
        return company;
    }

    public void setCompany(CompanyNode company) {
        this.company = company;
    }
}
