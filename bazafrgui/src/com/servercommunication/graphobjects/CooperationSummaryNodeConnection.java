package com.servercommunication.graphobjects;

import java.util.ArrayList;

public class CooperationSummaryNodeConnection {
    private PageInfo pageInfo;
    private ArrayList<CooperationSummaryNodeEdge> edges;

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public ArrayList<CooperationSummaryNodeEdge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<CooperationSummaryNodeEdge> edges) {
        this.edges = edges;
    }
}
