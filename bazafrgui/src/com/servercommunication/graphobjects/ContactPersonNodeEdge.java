package com.servercommunication.graphobjects;

public class ContactPersonNodeEdge {
    private ContactPersonNode node;
    private String cursor;

    public ContactPersonNode getNode() {
        return node;
    }

    public void setNode(ContactPersonNode node) {
        this.node = node;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
