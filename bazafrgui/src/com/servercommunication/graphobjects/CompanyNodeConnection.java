package com.servercommunication.graphobjects;

import java.util.ArrayList;

public class CompanyNodeConnection {
    private PageInfo pageInfo;
    private ArrayList<CompanyNodeEdge> edges;

    public ArrayList<CompanyNodeEdge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<CompanyNodeEdge> edges) {
        this.edges = edges;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }
}
