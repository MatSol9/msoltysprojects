package com.servercommunication.graphobjects;

public class DepartmentNodeEdge {
    private DepartmentNode node;
    private String cursor;

    public DepartmentNode getNode() {
        return node;
    }

    public void setNode(DepartmentNode node) {
        this.node = node;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
