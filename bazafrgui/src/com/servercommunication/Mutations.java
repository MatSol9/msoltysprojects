package com.servercommunication;

import com.servercommunication.mutationobjects.*;
import io.aexp.nodes.graphql.Argument;
import io.aexp.nodes.graphql.Arguments;
import io.aexp.nodes.graphql.GraphQLResponseEntity;

import java.util.Date;

public class Mutations {

    /**
     * Mutacja tworząca nową firmę w bazie
     * @param name nazwa firmy
     * @param description opis firmy
     * @param email email do firmy
     * @param phoneNumber numer telefonu
     * @param www strona internetowa
     * @param potential potencjał firmy
     * @throws NullPointerException Jak się wysypie to zwraca null
     */
    public void companyCreate(
            String name,
            String description,
            String email,
            String phoneNumber,
            String www,
            int potential
    ) throws NullPointerException {

        QMSender<CompanyCreate> qmSender= new QMSender<>(CompanyCreate.class,
                new Arguments(
                        "companyCreate",
                        new Argument("name", name),
                        new Argument("description", description),
                        new Argument("email", email),
                        new Argument("phoneNumber", phoneNumber),
                        new Argument("www", www),
                        new Argument("potential", potential)
                ),
                true,
                true
        );
        GraphQLResponseEntity<CompanyCreate> responseEntity = qmSender.getResponse();
    }

    /**
     * Funkcja usuwająca firmę
     * @param id ID firmy do usunięcia
     * @throws NullPointerException zwraca null jeśli coś się zepsuło
     */
    public void companyDelete(String id) throws NullPointerException {

        QMSender<CompanyDelete> qmSender = new QMSender<>(CompanyDelete.class,
                new Arguments(
                        "companyDelete",
                        new Argument("companyId", id)
                ),
                true,
                true
        );

        GraphQLResponseEntity<CompanyDelete> responseEntity = qmSender.getResponse();
    }

    /**
     * Ustawia osobę odpowiedzialną za firmę
     * @param staffId ID osoby do ustawienia
     * @param companyId firma która ma być dana tej osobie
     * @throws NullPointerException rzuca null jeśli się zepsuło
     */
    public void companySetResponsiblePerson(String staffId, String companyId) throws NullPointerException{
        QMSender<CompanySetResponsiblePerson> qmSender = new QMSender<>(CompanySetResponsiblePerson.class,
                new Arguments(
                        "companySetResponsiblePerson",
                        new Argument("companyId",companyId),
                        new Argument("staffId",staffId)
                ),
                true,
                true
        );
        GraphQLResponseEntity<CompanySetResponsiblePerson> responseEntity = qmSender.getResponse();
    }

    /**
     * funkcja updatująca firmę
     * @param id ID firmy do zaktualizowania
     * @param name nazwa firmy
     * @param description opis firmy
     * @param email email do firmy
     * @param phoneNumber numer telefonu
     * @param www strona internetowa firmy
     * @param potential potencjał firmy
     * @throws NullPointerException rzuca null jeśli coś się zepsuje
     */
    public void companyUpdate(
            String id,
            String name,
            String description,
            String email,
            String phoneNumber,
            String www,
            int potential
    ) throws NullPointerException{
        QMSender<CompanyUpdate> qmSender = new QMSender<>(CompanyUpdate.class,
                new Arguments(
                        "companyUpdate",
                        new Argument("companyId", id),
                        new Argument("name", name),
                        new Argument("description", description),
                        new Argument("email", email),
                        new Argument("phoneNumber", phoneNumber),
                        new Argument("www", www),
                        new Argument("potential", potential)
                ),
                true,
                true);
        GraphQLResponseEntity<CompanyUpdate> responseEntity = qmSender.getResponse();
    }

    /**
     * Funkcja tworząca historię kontaktu
     * @param companyId ID firmy która ma dostać historię
     * @param contactDate data kontaktu
     * @param notes notatki
     */
    public void contactHistoryCreate(String companyId, String contactDate, String notes){
        QMSender<ContactHistoryCreate> qmSender = new QMSender<>(
                ContactHistoryCreate.class,
                new Arguments(
                        "contactHistoryCreate",
                        new Argument("companyId", companyId),
                        new Argument("contactDate", contactDate),
                        new Argument("notes", notes)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja usuwająca historię kontaktu
     * @param contactHistoryId ID wpisu do historii który ma być usunięty usunięta
     */
    public void contactHistoryDelete(String contactHistoryId){
        QMSender<ContactHistoryDelete> qmSender = new QMSender<>(
                ContactHistoryDelete.class,
                new Arguments(
                        "contactHistoryDelete",
                        new Argument("contactHistoryId", contactHistoryId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja aktualizująca historię kontaktu
     * @param contactDate data kontaktu
     * @param contactHistoryId ID wpisu historii do zaktualizowania
     * @param notes notatki
     */
    public void contactHistoryUpdate(String contactDate, String contactHistoryId, String notes){
        QMSender<ContactHistoryUpdate> qmSender = new QMSender<>(
                ContactHistoryUpdate.class,
                new Arguments(
                        "contactHistoryUpdate",
                        new Argument("contactDate", contactDate),
                        new Argument("contactHistoryId", contactHistoryId),
                        new Argument("notes", notes)
                ),
                true,
                true
        );
    }

    /**
     * funkcja tworząca osobę kontaktową
     * @param companyId id firmy
     * @param email email do nowej osoby kontaktowej
     * @param name nazwa osoby kontaktowej
     * @param notes notatki
     * @param phoneNumber numer telefonu
     */
    public void companyContactPersonCreate(String companyId, String email, String name, String notes, String phoneNumber){
        QMSender<CompanyContactPersonCreate> qmSender = new QMSender<>(
                CompanyContactPersonCreate.class,
                new Arguments(
                        "companyContactPersonCreate",
                        new Argument("companyId", companyId),
                        new Argument("email", email),
                        new Argument("name", name),
                        new Argument("name", notes),
                        new Argument("name", phoneNumber)
                ),
                true,
                true
        );
    }

    /**
     * funkcja aktualizująca osobę kontaktową
     * @param contactPersonId id osoby kontaktowej
     * @param email email do osoby kontaktowej
     * @param name nazwa osoby kontaktowej
     * @param notes notatki
     * @param phoneNumber numer telefonu
     */
    public void companyContactPersonUpdate(String contactPersonId, String email, String name, String notes, String phoneNumber){
        QMSender<CompanyContactPersonUpdate> qmSender = new QMSender<>(
                CompanyContactPersonUpdate.class,
                new Arguments(
                        "companyContactPersonUpdate",
                        new Argument("contactPersonId", contactPersonId),
                        new Argument("email", email),
                        new Argument("name", name),
                        new Argument("name", notes),
                        new Argument("name", phoneNumber)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja usuwająca osobę kontaktową
     * @param contactPersonId id osoby do usunięcia
     */
    public void companyContactPersonDelete(String contactPersonId){
        QMSender<CompanyContactPersonDelete> qmSender = new QMSender<>(
                CompanyContactPersonDelete.class,
                new Arguments(
                        "companyContactPersonDelete",
                        new Argument("contactPersonId", contactPersonId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja tworząca podsumowanie współpracy
     * @param companyId ID firmy
     * @param results resultaty
     * @param summary podsumowanie
     * @param year rok
     */
    public void cooperationSummaryCreate(String companyId, String results, String summary, int year){
        QMSender<CooperationSummaryCreate> qmSender = new QMSender<>(
                CooperationSummaryCreate.class,
                new Arguments(
                        "cooperationSummaryCreate",
                        new Argument("companyId", companyId),
                        new Argument("results", results),
                        new Argument("summary", summary),
                        new Argument("year", year)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja usuwająca podsumowanie współpracy
     * @param cooperationSummaryId ID podsumowania do usunięcia
     */
    public void cooperationSummaryDelete(String cooperationSummaryId){
        QMSender<CooperationSummaryDelete> qmSender = new QMSender<>(
                CooperationSummaryDelete.class,
                new Arguments(
                        "cooperationSummaryDelete",
                        new Argument("cooperationSummaryId", cooperationSummaryId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja aktualizująca podsumowanie współpracy
     * @param cooperationSummaryId ID podsumowania do zaktualizowania
     * @param results rezultaty
     * @param summary podsumowanie
     */
    public void cooperationSummaryUpdate(String cooperationSummaryId, String results, String summary){
        QMSender<CooperationSummaryUpdate> qmSender = new QMSender<>(
                CooperationSummaryUpdate.class,
                new Arguments(
                        "cooperationSummaryUpdate",
                        new Argument("cooperationSummaryId", cooperationSummaryId),
                        new Argument("results", results),
                        new Argument("summary", summary)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja tworząca dział
     * @param name nazwa działu do utworzenia
     */
    public void departmentCreate(String name){
        QMSender<DepartmentCreate> qmSender = new QMSender<>(
                DepartmentCreate.class,
                new Arguments(
                        "departmentCreate",
                        new Argument("name", name)
                ),
                true,
                true
        );
    }


    /**
     * Funkcja usuwająca dział
     * @param departmentId ID działu
     */
    public void departmentDelete(String departmentId){
        QMSender<DepartmentDelete> qmSender = new QMSender<>(
                DepartmentDelete.class,
                new Arguments(
                        "departmentDelete",
                        new Argument("departmentId", departmentId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja ustawiająca przełożonego działu
     * @param departmentId ID działu
     * @param staffId ID przełożonego
     */
    public void departmentSetSupervisor(String departmentId, String staffId){
        QMSender<DepartmentSetSupervisor> qmSender = new QMSender<>(
                DepartmentSetSupervisor.class,
                new Arguments(
                        "departmentSetSupervisor",
                        new Argument("departmentId", departmentId),
                        new Argument("staffId", staffId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja tworząca członka zespołu
     * @param department dział
     * @param email email
     * @param name imię
     * @param password hasło
     * @param rank ranga
     * @param studyYearStart rok studiów
     * @param surname nazwisko
     * @param username login użytkownika
     */
    public void staffCreate(
            String department,
            String email,
            String name,
            String password,
            String rank,
            int studyYearStart,
            String surname,
            String username
    ){
        QMSender<StaffCreate> qmSender = new QMSender<>(
                StaffCreate.class,
                new Arguments(
                        "staffCreate",
                        new Argument("department", department),
                        new Argument("email", email),
                        new Argument("name", name),
                        new Argument("password", password),
                        new Argument("rank", rank),
                        new Argument("studyYearStart", studyYearStart),
                        new Argument("surname", surname),
                        new Argument("username", username)
                ),
                true,
                true);
    }

    /**
     * Funkcja zmieniająca rangę członka zespołu
     * @param rank ranga do ustawienia
     * @param staffId ID członka zespołu
     */
    public void staffChangeRank(String rank, String staffId){
        QMSender<StaffChangeRank> qmSender = new QMSender<>(
                StaffChangeRank.class,
                new Arguments(
                        "staffChangeRank",
                        new Argument("rank", rank),
                        new Argument("staffId", staffId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja przenosząca osobę do innego działu
     * @param department dział docelowy
     * @param staffId ID osoby do przeniesienia
     */
    public void staffChangeDepartment(String department, String staffId){
        QMSender<StaffChangeDepartment> qmSender = new QMSender<>(
                StaffChangeDepartment.class,
                new Arguments(
                        "staffChangeDepartment",
                        new Argument("department", department),
                        new Argument("staffId", staffId)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja zmieniająca dane użytkownika
     * @param staffId ID osoby do przeniesienia
     * @param name imię osoby
     * @param surname nazwisko osoby
     * @param email email osoby
     * @param studyYearStart rok rozpoczęcia studiów
     */
    public void staffUpdate(String staffId, String name, String surname, String email, int studyYearStart){
        QMSender<StaffUpdate> qmSender = new QMSender<>(
                StaffUpdate.class,
                new Arguments(
                        "staffUpdate",
                        new Argument("email", email),
                        new Argument("staffId", staffId),
                        new Argument("name", name),
                        new Argument("surname", surname),
                        new Argument("studyYearStart", studyYearStart)
                ),
                true,
                true
        );
    }

    /**
     * Funkcja zmieniająca hasło użytkownika
     * @param staffId ID osoby do przeniesienia
     * @param newPassword nowe hasło
     * @param oldPassword stare hasło
     */
    public boolean staffChangePassword(String staffId, String newPassword, String oldPassword){
        QMSender<StaffChangePassword> qmSender = new QMSender<>(
                StaffChangePassword.class,
                new Arguments(
                        "staffChangePassword",
                        new Argument("staffId", staffId),
                        new Argument("newPassword", newPassword),
                        new Argument("oldPassword", oldPassword)
                ),
                true,
                true
        );
        try{
            return qmSender.getResponse().getResponse().getStaff().getName() != null;
        }
        catch(NullPointerException a){
            System.out.println(a.getMessage());
            return false;
        }

    }
}
