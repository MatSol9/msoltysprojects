package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.DepartmentNodeConnection;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(name = "departmentsAll")
public class DepartmentsAll extends DepartmentNodeConnection {
}
